$(document).ready(function(){

    // Slide Photo 
    $('.thumbnail span').click(function(){
         var imgSrc = $(this).find('img').attr('src');
         $('.main-photo img').attr('src', imgSrc);
    });


    // Show modal 
    $('.js-open-modal').click(function(e){
        e.preventDefault();
        $('body').addClass('modal-open');
        $('.bg-modal,.modal').addClass('active');
    });
    // Hide modal 
    $('.js-close-modal').click(function(e){
        e.preventDefault();
        $('body').removeClass('modal-open');
        $('.bg-modal,.modal').removeClass('active');
    });

    // Menu  

    $(window).on('load resize', function(){ 
        var wdWidth = $(window).width();
        
        $('.sub-menu').removeClass('active');
        $('#header').css('height', '125px');

        if (wdWidth > 768){
            $('.dropdown').hover(function(){
                var headerHeight = $(this).find('.sub-menu').outerHeight() + 125;
                $('#header').css('height', headerHeight);
            },function(){
                $('#header').css('height', '125px');
            });
        }else{

            var click = true;
            $('.nav li.dropdown>a').click(function(e){
                e.preventDefault();

                var headerHeight = $(this).next('.sub-menu').outerHeight() + 125;

                if(click == true){
                    $('#header').css('height', headerHeight);
                    $(this).next('.sub-menu').addClass('active');
                    click = false;
                }else{
                    $('#header').css('height', '125px');
                    $(this).next('.sub-menu').removeClass('active');
                    click = true;
                }
            });
        }
    });

});